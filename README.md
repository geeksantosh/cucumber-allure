# Allure Cucumber

[![Yard Docs](https://img.shields.io/badge/yard-docs-blue.svg)](https://www.rubydoc.info/gems/allure-cucumber)


## Cucumber versions

allure-cucumber versions <= 2.13.4 support only cucumber 3 and lower\
allure-cucumber versions >= 2.13.5 only support cucumber 4 and are not backwards compatible with cucumber 3 and lower

## Installation

Add this line to your application's Gemfile:

```ruby
gem 'allure-cucumber', '~> 2.21', '>= 2.21.2'
```

And then execute:

```bash
  bundle install
```

Or install it yourself as:

```bash
  gem install allure-cucumber
```

Require in "support/env.rb":

```ruby
require "allure-cucumber"
```

## Configuration

Common allure configuration is set via `AllureCucumber.configure` method. To change it, add the following in `features/support/env.rb` file:

```ruby
require "allure-cucumber"

AllureCucumber.configure do |config|
  config.results_directory = "feature/report/allure-results" # Specify the directory to store the report data
  config.clean_results_directory = true # Clean the results directory before each test run
  config.logging_level = Logger::INFO # Set the logging level (INFO by default)
  config.logger = Logger.new($stdout, Logger::DEBUG)
  config.environment = "staging"

  # these are used for creating links to bugs or test cases where {} is replaced with keys of relevant items
  config.link_tms_pattern = "http://www.jira.com/browse/{}"
  config.link_issue_pattern = "http://www.jira.com/browse/{}"

  # additional metadata
  # environment.properties
  config.environment_properties = {
    custom_attribute: "foo"
  }
  # categories.json
  config.categories = File.new("my_custom_categories.json")
end
```

By default, allure-cucumber will analyze your cucumber tags looking for Test Management, Issue Management, and Severity tag as well
as custom tags for grouping tests in to epics, features and stories in Behavior tab of report. Links to TMS and ISSUE and test severity will be displayed in the report.

By default these prefixes are used:

```ruby
    DEFAULT_TMS_PREFIX      = 'TMS:'
    DEFAULT_ISSUE_PREFIX    = 'ISSUE:'
    DEFAULT_SEVERITY_PREFIX = 'SEVERITY:'
    DEFAULT_EPIC_PREFIX     = 'EPIC:'
    DEFAULT_FEATURE_PREFIX  = 'FEATURE:'
    DEFAULT_STORY_PREFIX    = 'STORY:'
```

Example:

```gherkin
  @SEVERITY:trivial @ISSUE:YZZ-100 @TMS:9901 @EPIC:custom-epic
  Scenario: Leave First Name Blank
    When I register an account without a first name
    Then exactly (1) [validation_error] should be visible
```

You can configure these prefixes as well as tms and issue tracker urls like this:

```ruby
AllureCucumber.configure do |config|
  config.tms_prefix      = 'HIPTEST--'
  config.issue_prefix    = 'JIRA++'
  config.severity_prefix = 'URGENCY:'
  config.epic_prefix = 'epic:'
  config.feature_prefix = 'feature:'
  config.story_prefix = 'story:'
end
```

Example:

```gherkin
  @URGENCY:critical @JIRA++YZZ-100 @HIPTEST--9901 @epic:custom-epic
  Scenario: Leave First Name Blank
    When I register an account without a first name
    Then exactly (1) [validation_error] should be visible
```

Additional special tags exists for setting status detail of test scenarios, allure will pick up following tags: `@flaky`, `@known` and `@muted`

## Usage

Use `--format AllureCucumber::CucumberFormatter --out where/you-want-results` while running cucumber or add it to `cucumber.yml`. Note that cucumber `--out` option overrides `results_directory` set via `Allure.configure` method.

You can also manually attach screenshots and links to test steps and test cases by interacting with allure lifecycle directly. For more info check out `allure-ruby-commons`

```ruby
require "allure-cucumber"

Allure.add_attachment(name: "attachment", source: "Some string", type: Allure::ContentType::TXT, test_case: true)
Allure.add_description("Test Description")
Allure.tag("My Tag")
```


### Example project

[Cucumber Example](https://github.com/allure-examples/allure-cucumber-example)

## Checking Allure Report
### Steps
1. Ensure that you have generated the Allure report after running your Cucumber tests. Make sure the report generation is properly configured in your Cucumber test framework.
2. Locate the directory where the Allure report is generated. By default, it is usually created in a directory named allure-results or target/allure-results within your project's directory.
3. Install the Allure command-line tool if you haven't already. Refer to the official Allure framework website for installation instructions suitable for your platform.
4. Open a terminal or command prompt and navigate to the directory containing the Allure report. Use the cd command to change directories.
5. Once inside the directory, run the following command:
```allure serve```\
This command will start a local web server and automatically open the Allure report in your default web browser.

6. The Allure report will now be displayed in your web browser, providing an interactive view of the test execution results, statistics, and other relevant information.

You can explore the report to view detailed information about the executed Cucumber scenarios, their statuses, associated steps, screenshots, logs, and any other artifacts or attachments linked to the test execution.

Using "allure serve" eliminates the need to manually generate and open the report, as it does both steps for you in a single command.