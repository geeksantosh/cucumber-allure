require 'selenium-webdriver'
require "allure-cucumber"

Given("I am on the Google homepage") do
  @driver = Selenium::WebDriver.for :chrome
  @driver.navigate.to "https://www.google.com"
end

Then("I should see the Google logo") do
  Allure.description_html("HTML Desc")
  Allure.tag("Tag")
  Allure.epic("EPIC")
  Allure.add_description("This is my test description")
  logo = @driver.find_element(:class, 'lnXdpd')
  expect(logo.displayed?).to be true
end
