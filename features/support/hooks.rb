Before do |scenario|
  puts "HI"
end


After do |scenario|
  if scenario.failed?
    timestamp = Time.now.strftime('%Y%m%d_%H%M%S')
    screenshot_name = "#{scenario.name}_#{timestamp}.png"
    screenshot_path = "features/reports/allure-results#{screenshot_name}"
    file = @driver.save_screenshot(screenshot_path)

    Allure.add_attachment(
      name: "#{screenshot_name}",
      source: File.open(file, 'rb'),
      type: Allure::ContentType::PNG,
      test_case: true)
  end
end