require 'rspec/expectations'
require 'allure-cucumber'
require 'allure-rspec'
require 'socket'
require 'watir'

# Configure Allure-Cucumber
AllureCucumber.configure do |config|
  config.results_directory = "features/reports/allure-results" # Specify the directory to store the report data
  config.clean_results_directory = false # Clean the results directory before each test run
  config.logging_level = Logger::INFO # Set the logging level (INFO by default)

  # Add Environment Information
  config.environment_properties = {
    environment: Socket.gethostname,
    os: RbConfig::CONFIG['host_os'],
    cucumber_version: Cucumber::VERSION,
    watir_version: Watir::VERSION
  }

end

AllureCucumber.configure do |c|
  c.tms_prefix      = '@HIPTEST--'
  c.issue_prefix    = '@JIRA++'
  c.severity_prefix = '@URGENCY:'
end

